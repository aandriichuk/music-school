﻿namespace DynamicDevs.MusicSchool.AudioService.PeakProvider
{
    using System.Linq;

    public class MaxPeakProvider : PeakProvider
    {
        public override PeakInfo GetNextPeak()
        {
            var samplesRead = this.Provider.Read(this.ReadBuffer, 0, this.ReadBuffer.Length);
            var max = this.ReadBuffer.Take(samplesRead).Max();
            var min = this.ReadBuffer.Take(samplesRead).Min();
            return new PeakInfo(min, max);
        }
    }
}