﻿namespace DynamicDevs.MusicSchool.AudioService.PeakProvider
{
    using NAudio.Wave;

    public interface IPeakProvider
    {
        void Init(ISampleProvider reader, int samplesPerPixel);
        PeakInfo GetNextPeak();
    }
}