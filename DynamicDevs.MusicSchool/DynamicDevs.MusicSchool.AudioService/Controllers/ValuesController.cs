﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace DynamicDevs.MusicSchool.AudioService.Controllers
{
    using DynamicDevs.MusicSchool.AudioService.PeakProvider;

    using NAudio.Wave;

    [Route("api/get-wave-peacks")]
    public class WaveController : Controller
    {
        // GET api/values
        [HttpGet]
        public List<PeakInfo> Get()
        {
            int canvasWidth = 800;
            List<PeakInfo> peacks = new List<PeakInfo>();
            var path = @"C:\Projects\MusicSchool\DynamicDevs.MusicSchool\DynamicDevs.MusicSchool.WinAudio.Tests\Audio\Tyler Bates - Plastic Heart.mp3";
            using (var reader = new AudioFileReader(path))
            {
                int bytesPerSample = reader.WaveFormat.BitsPerSample / 8;
                var samples = reader.Length / bytesPerSample;
                var samplesPerPixel = (int)(samples / canvasWidth);
                var peakProvider = new MaxPeakProvider();
                peakProvider.Init(reader, samplesPerPixel);

                for (var x = 0; x < canvasWidth; x++)
                {
                    var peak = peakProvider.GetNextPeak();
                    peacks.Add(peak);
                }
            }

            return peacks;
        }
    }
}
